/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { router } from '@kit.ArkUI'

@ComponentV2
struct SideBar {
  build() {
    Column({space: 10}) {
      Row({space: 5}) {
        Image($r('app.media.image')).objectFit(ImageFit.Contain)
          .width("29%").height("53%")
          .margin({left: 16, right: 16})
        Column({space: 10}) {
          Text("Over-Scroll Demo App").fontSize(30).fontColor(Color.White).width("60%")
          Text("EverythingMe").fontSize(22).fontColor(Color.White)
        }.alignItems(HorizontalAlign.Start)
      }
      .height("20%").width("100%")
      .backgroundColor("#ffffbb33")

      Row({space: 15}) {
        Image($r('app.media.ic_recycler')).objectFit(ImageFit.Contain)
          .width("10%").height("65%").margin({left: 8})
        Text("RecyclerView Demo").fontSize(14).fontColor("#727171")
      }.height("6%").width("100%")
      .onClick((event?: ClickEvent) => {
        router.pushUrl({ url: 'pages/RecyclerViewAttachDemo' })
      })

      Row({space: 15}) {
        Image($r('app.media.ic_recycler')).objectFit(ImageFit.Contain)
          .width("10%").height("65%").margin({left: 8})
        Text("RecyclerView - St.Grid Demo").fontSize(14).fontColor("#727171")
      }.height("6%").width("100%")
      .onClick((event?: ClickEvent) => {
        router.pushUrl({ url: 'pages/RecyclerViewStaggeredGridDemo' })
      })

      Row({space: 15}) {
        Image($r('app.media.ic_grid')).objectFit(ImageFit.Contain)
          .width("10%").height("65%").margin({left: 8})
        Text("Grid Demo").fontSize(14).fontColor("#727171")
      }.height("6%").width("100%")
      .onClick((event?: ClickEvent) => {
        router.pushUrl({ url: 'pages/GridViewDemo' })
      })

      Row({space: 15}) {
        Image($r('app.media.ic_list')).objectFit(ImageFit.Contain)
          .width("10%").height("65%").margin({left: 8})
        Text("ListView Demo").fontSize(14).fontColor("#727171")
      }.height("6%").width("100%")
      .onClick((event?: ClickEvent) => {
        router.pushUrl({ url: 'pages/ListViewDemo' })
      })

      Row({space: 15}) {
        Image($r('app.media.ic_scroller')).objectFit(ImageFit.Contain)
          .width("10%").height("65%").margin({left: 8})
        Text("ScrollView Demo").fontSize(14).fontColor("#727171")
      }.height("6%").width("100%")
      .onClick((event?: ClickEvent) => {
        router.pushUrl({ url: 'pages/ScrollViewDemo' })
      })

      Row({space: 15}) {
        Image($r('app.media.ic_view_pager')).objectFit(ImageFit.Contain)
          .width("10%").height("65%").margin({left: 8})
        Text("ViewPager Demo").fontSize(14).fontColor("#727171")
      }.height("6%").width("100%")
      .onClick((event?: ClickEvent) => {
        router.pushUrl({ url: 'pages/ViewPagerDemo' })
      })

      Row({space: 15}) {
        Image($r('app.media.ic_nested')).objectFit(ImageFit.Contain)
          .width("10%").height("65%").margin({left: 8})
        Text("NestedScrollView Demo").fontSize(14).fontColor("#727171")
      }.height("6%").width("100%")
      .onClick((event?: ClickEvent) => {
        router.pushUrl({ url: 'pages/NestedScrollViewDemo' })
      })

      Row({space: 15}) {
        Image($r('app.media.ic_misc')).objectFit(ImageFit.Contain)
          .width("10%").height("65%").margin({left: 8})
        Text("Misc-Views  Demo").fontSize(14).fontColor("#727171")
      }.height("6%").width("100%")
      .onClick((event?: ClickEvent) => {
        router.pushUrl({ url: 'pages/MiscViewsDemo' })
      })
    }
    .height("100%").width("85%")
    .backgroundColor(Color.White)
  }
}

export default SideBar