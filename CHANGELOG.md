## 2.1.0
- 更新正式版本2.1.0

## 2.1.0-rc.1
- 将过时方法 onScroll 修改为 onDidScroll

## 2.1.0-rc.0
- 升级状态管理器V2

## 2.0.1
- ArkTS语法适配

## 2.0.0

- 包管理工具由npm切换为ohpm。
- 适配DevEco Studio:3.1Beta2(3.1.0.400), SDK:API9 Release(3.2.11.9)。

## 1.1.1

- 适配DevEco Studio 3.1Beta1版本。
- 适配OpenHarmony SDK API version 9版本。

## 1.1.0

1. 名称由overscroll-decor-ets修改为overscroll-decor。
2. 旧的包@ohos/overscroll-decor-ets已不维护，请使用新包@ohos/overscroll-decor。

## 1.0.1

- api8升级到api9，并将FA模型转换为stage模型

## 1.0.0

一个iOS风格的边缘滚动效果,支持以下视图：
RecyclerView,
ListView,
GridView,
ViewPager,
ScrollView, HorizontalScrollView,
Any View - Text, Image